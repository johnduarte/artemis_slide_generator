import logging
import math
import os
import re
from inspect import getsourcefile

from googleapiclient.discovery import build
from openpyxl import load_workbook
from openpyxl.cell import Cell
from openpyxl.drawing.image import Image
from openpyxl.styles import Alignment
from openpyxl.utils import get_column_letter
from openpyxl.utils.exceptions import InvalidFileException
from openpyxl.workbook.workbook import Workbook
from openpyxl.worksheet.dimensions import ColumnDimension, DimensionHolder
from openpyxl.worksheet.worksheet import Worksheet
from PIL import Image as PIL_Image
from PIL import UnidentifiedImageError

from artemis_sg import app_creds, item, items
from artemis_sg.config import CFG
from artemis_sg.isbn import validate_isbn
from artemis_sg.vendor import vendor

MODULE = os.path.splitext(os.path.basename(__file__))[0]


def get_worksheet(wb_obj: Workbook, worksheet: str | None = None) -> Worksheet:
    """
    Get openypyxl Worksheet object matching name of given `worksheet`.
    Will return first Worksheet in Workbook if lookup `worksheet` string is not given.

    :param wb_obj: openpyxl Workbook
    :param worksheet: name of worksheet to return
    """
    ws = wb_obj.worksheets[0] if not worksheet else wb_obj[worksheet]
    return ws


def get_sheet_keys(ws: Worksheet) -> list[str]:
    """
    Get list of column headers from worksheet

    :param ws: openpyxl Worksheet
    :returns: list of column headers in uppercase
    """
    headers = next(ws.values)
    return [x.upper() if isinstance(x, str) else x for x in headers]


def shift_col(ws: Worksheet, col_key: str, target_idx: int) -> None:
    """
    Move worksheet column with given `col_key` to `target_idx` column
    and shift remaining column over to accomodate it.

    Example:
    | GIVEN worksheet columns: ('foo', 'bar', 'ISBN-13', 'baz', 'Order')
    | AND col_key = 'ISBN-13'
    | AND target_idx = 1
    | THEN new columns are: ('ISBN-13', 'foo', 'bar', 'baz', 'Order')

    :param ws: openpyxl Worksheet
    :param col_key: column header key to move
    :param target_idx: column position to move to (1 indexed)
    """
    ws.insert_cols(target_idx)
    sheet_keys = get_sheet_keys(ws)
    sheet_key_idx = sheet_keys.index(col_key) + 1  # for openpyxl
    sheet_key_idx_ltr = get_column_letter(sheet_key_idx)
    col_delta = target_idx - sheet_key_idx
    ws.move_range(
        f"{sheet_key_idx_ltr}1:{sheet_key_idx_ltr}{ws.max_row}", rows=0, cols=col_delta
    )
    ws.delete_cols(sheet_key_idx)


def freeze_first_row(worksheet: Worksheet) -> None:
    """
    Freeze first row of `worksheet`

    :param ws: openpyxl Worksheet
    """
    logging.info("Freezing first row of worksheet.")
    worksheet.views.sheetView[0].topLeftCell = "A1"
    worksheet.freeze_panes = "A2"


def create_col(ws: Worksheet, col_key: str, target_idx: int) -> None:
    """
    Create worksheet column with given `col_key` at `target_idx` column.

    :param ws: openpyxl Worksheet
    :param col_key: column header key to move
    :param target_idx: column position to move to (1 indexed)
    """
    ws.insert_cols(target_idx)
    col_header = f"{get_column_letter(target_idx)}1"
    ws[col_header] = col_key.title()


def sequence_worksheet(ws: Worksheet, col_order: list[str], isbn_key: str) -> None:
    """
    Reorder the columns of the worksheet `ws` in accordance with the given
    `col_order`.  Columns not defined in the `col_order` will be appended
    to the end of the worksheet columns.

    :param ws: openpyxl Worksheet
    :param col_order: list of column keys in order of preference
    :param isbn_key: the key in `col_order` that corresponds with ISBN
    """
    sheet_keys = get_sheet_keys(ws)
    for i, key_name in enumerate(col_order):
        order_idx = i + 1  # for openpyxl
        if key_name == "ISBN":
            key_name = isbn_key  # noqa: PLW2901
        if key_name in sheet_keys:
            shift_col(ws, key_name, order_idx)
        else:
            create_col(ws, key_name, order_idx)


def size_sheet_cols(
    ws: Worksheet,
    isbn_key: str,
    fmt_opts: dict = CFG["asg"]["spreadsheet"]["sheet_image"],
) -> None:
    """
    Resize the width of the `ws` worksheet columns in accordance with the
    preferences defined in `fmt_opts`.

    :param ws: openpyxl Worksheet
    :param isbn_key: the key worksheet column headers that corresponds with ISBN
    :param fmt_opts: dictionary of formatting options
    """
    fmt_opts.setdefault("col_buffer", 1.25)
    fmt_opts.setdefault("max_col_width", 50)
    fmt_opts.setdefault("isbn_col_width", 13)
    fmt_opts.setdefault("image_col_width", 20)
    dim_holder = DimensionHolder(worksheet=ws)
    sheet_keys = get_sheet_keys(ws)
    for i, key_name in enumerate(sheet_keys):
        col_idx = i + 1  # for openpyxl
        col_idx_ltr = get_column_letter(col_idx)
        width = (
            max(len(str(cell.value)) for cell in ws[col_idx_ltr])
            * fmt_opts["col_buffer"]
        )
        width = min(width, fmt_opts["max_col_width"])
        dim_holder[col_idx_ltr] = ColumnDimension(ws, index=col_idx_ltr, width=width)
        if key_name == isbn_key:
            dim_holder[col_idx_ltr] = ColumnDimension(
                ws,
                index=col_idx_ltr,
                width=math.ceil(fmt_opts["isbn_col_width"] * fmt_opts["col_buffer"]),
            )
        if key_name == "IMAGE":
            dim_holder[col_idx_ltr] = ColumnDimension(
                ws,
                index=col_idx_ltr,
                width=fmt_opts["image_col_width"],
            )

    ws.column_dimensions = dim_holder


def insert_image(
    image_directory: str,
    ws: Worksheet,
    isbn_cell: Cell,
    image_cell: Cell,
    image_row_height: int = CFG["asg"]["spreadsheet"]["sheet_image"][
        "image_row_height"
    ],
) -> None:
    """
    Insert an image into a Worksheet cell associated with an ISBN

    :param image_directory: path to the directory where item images are stored
    :param ws: openpyxl Worksheet
    :param isbn_cell: `Cell` object containing the ISBN number of the item
    :param image_cell: `Cell` object that image should be inserted into
    :param image_row_height: Height to set row containing image
    """
    namespace = f"{MODULE}.{insert_image.__name__}"
    isbn = validate_isbn(isbn_cell.value)
    # Set row height
    row_dim = ws.row_dimensions[image_cell.row]
    row_dim.height = image_row_height

    # Insert image into cell
    filename = f"{isbn}.jpg"
    filepath = os.path.join(image_directory, filename)
    logging.debug(f"{namespace}: Attempting to insert '{filepath}'.")
    if os.path.isfile(filepath):
        img = Image(filepath)
        ws.add_image(img, f"{image_cell.column_letter}{image_cell.row}")
        logging.info(f"{namespace}: Inserted '{filepath}'.")


def sheet_image(
    vendor_code: str,
    workbook: str,
    worksheet: str,
    image_directory: str,
    out: str,
    col_order: list[str] = CFG["asg"]["spreadsheet"]["sheet_image"]["col_order"],
) -> Worksheet:
    """
    Create a new spreadsheet file based on a given `workbook`/`worksheet`
    with an added column for a product image added for each ISBN in the `worksheet`.

    Images are obtained from the given `image_directory` and are expected to be named
    in the the pattern `{isbn}.jpg`.

    The spreadsheet columns are reordered in accordance with the given
    `col_order` parameter.

    The new spreadsheet is saved with the filename(path) given by the `out` parameter.

    :param vendor_code: Vendor code used to lookup isbn_key (See `vendor`)
    :param workbook: path to Excel workbook file
    :param worksheet: name of worksheet in workbook file
    :param image_directory: path to the directory where item images are stored
    :param out: path where the new spreadsheet will be saved
    :param col_order: list of column keys in order of preference
    :returns: Worksheet updated with item images
    """
    namespace = f"{MODULE}.{sheet_image.__name__}"

    isbn_key = vendor(vendor_code).isbn_key
    logging.debug(f"{namespace}: Setting ISBN_KEY to '{isbn_key}'.")

    # Load worksheet
    logging.info(f"{namespace}: Workbook is {workbook}")
    wb = load_workbook(workbook)
    ws = get_worksheet(wb, worksheet)
    logging.info(f"{namespace}: Worksheet is {ws.title}")

    sequence_worksheet(ws, col_order, isbn_key)
    size_sheet_cols(ws, isbn_key)

    # Prepare "IMAGE" column
    sk = get_sheet_keys(ws)
    try:
        img_idx = sk.index("IMAGE") + 1
        img_idx_ltr = get_column_letter(img_idx)
    except ValueError as e:
        logging.error(f"{namespace}: Err finding 'IMAGE' column in sheet '{workbook}'.")
        logging.error("Aborting.")
        raise e
    try:
        isbn_idx = sk.index(isbn_key) + 1
        isbn_idx_ltr = get_column_letter(isbn_idx)
    except ValueError as e:
        logging.error(
            f"{namespace}: Err, no '{isbn_key}' column in sheet '{workbook}'."
        )
        logging.error("Aborting.")
        raise e

    for i in range(1, ws.max_row):
        isbn_cell = ws[f"{isbn_idx_ltr}{i+1}"]
        image_cell = ws[f"{img_idx_ltr}{i+1}"]
        # Format to center content
        image_cell.alignment = Alignment(horizontal="center")
        insert_image(image_directory, ws, isbn_cell, image_cell)

    freeze_first_row(ws)

    # Save workbook
    wb.save(out)

    return ws


def validate_qty(qty: str | int | float) -> str:
    """Cast given value into a string integer or None if unable"""
    namespace = f"{MODULE}.{validate_qty.__name__}"
    try:
        valid_qty = str(int(qty)).strip()
    except Exception as e:
        logging.error(f"{namespace}: Err reading Order qty '{qty}', err: '{e}'")
        valid_qty = None
    return valid_qty


def get_order_items(
    vendor_code: str, workbook: str, worksheet: str
) -> list[tuple[str, str]]:
    """
    Get a list of (ISBN, Quantity) pairs for items from the "Order" column of
    the given `workbook`/`worksheet`.

    :param vendor_code: Vendor code used to lookup isbn_key (See `vendor`)
    :param workbook: path to Excel workbook file
    :param worksheet: name of worksheet in workbook file
    """
    namespace = f"{MODULE}.{get_order_items.__name__}"

    order_items = []
    try:
        order_col = CFG["asg"]["spreadsheet"]["order"]["order_col"].upper()
    except AttributeError:
        logging.error(f"{namespace}: No order column set in config.toml")
        order_col = ""

    # get vendor info from database
    logging.debug(f"{namespace}: Instantiate vendor.")
    vendr = vendor(vendor_code)

    isbn_key = vendr.isbn_key
    logging.debug(f"{namespace}: Setting ISBN_KEY to '{isbn_key}'.")

    # Load worksheet
    logging.info(f"{namespace}: Workbook is {workbook}")
    wb = load_workbook(workbook)
    ws = get_worksheet(wb, worksheet)
    logging.info(f"{namespace}: Worksheet is {ws.title}")

    # Find Isbn and Order column letters
    row01 = ws[1]
    for cell in row01:
        if cell.value == isbn_key:
            isbn_column_letter = cell.column_letter
        if cell.value.upper() == order_col:
            order_column_letter = cell.column_letter

    for row in ws.iter_rows(min_row=2):
        for cell in row:
            if cell.column_letter == isbn_column_letter:
                isbn_cell = cell
            if cell.column_letter == order_column_letter:
                order_cell = cell
        # Validate ISBN
        isbn = validate_isbn(isbn_cell.value)
        # Validate Order Qty
        qty = validate_qty(order_cell.value)
        if not isbn and not qty:
            continue
        order_items.append((isbn, qty))

    return order_items


def _mkthumb(
    big_file: str,
    thumb_file: str,
    thumb_width: str = CFG["asg"]["spreadsheet"]["mkthumbs"]["width"],
    thumb_height: str = CFG["asg"]["spreadsheet"]["mkthumbs"]["height"],
) -> None:
    """
    Create an image file of `big_file` at path `thumb_file` of
    dimensions (`thumb_width`, `thumb_height`).

    :param big_file: path of image file to create thumbnail of
    :param thumb_file: path where image file of thumbnail should be saved
    :thumb_width: width (in pixels) that thumbnail should be
    :thumb_height: height (in pixels) that thumbnail should be
    """
    namespace = f"{MODULE}.{_mkthumb.__name__}"

    # validate big_file, delete if invalid
    try:
        fg = PIL_Image.open(big_file)
    except UnidentifiedImageError:
        logging.error(f"{namespace}: Err reading '{big_file}', deleting it.")
        os.remove(big_file)
        return
    # don't remake thumbnails
    if os.path.isfile(thumb_file):
        return
    here = os.path.dirname(getsourcefile(lambda: 0))
    data = os.path.abspath(os.path.join(here, "data"))
    logo = os.path.join(data, "artemis_logo.png")
    logging.debug(f"{namespace}: Found image for thumbnail background at '{logo}'")
    back = PIL_Image.open(logo)
    bk = back.copy()
    fg.thumbnail((thumb_width, thumb_height))
    size = (int((bk.size[0] - fg.size[0]) / 2), int((bk.size[1] - fg.size[1]) / 2))
    bk.paste(fg, size)
    logging.debug(f"{namespace}: Attempting to save thumbnail '{thumb_file}'")
    bkn = bk.convert("RGB")
    bkn.save(thumb_file)
    logging.info(f"{namespace}: Successfully created thumbnail '{thumb_file}'")


def mkthumbs(image_directory: str) -> None:
    """
    Create thumbnail image files of all images in given `image_directory`

    :param image_directory: path location of image files
    """
    namespace = f"{MODULE}.{mkthumbs.__name__}"

    sub_dir = "thumbnails"
    thumb_dir = os.path.join(image_directory, sub_dir)
    logging.debug(f"{namespace}: Defining thumbnail directory as '{thumb_dir}'")
    if not os.path.isdir(thumb_dir):
        logging.debug(f"{namespace}: Creating directory '{thumb_dir}'")
        os.mkdir(thumb_dir)
        if os.path.isdir(thumb_dir):
            logging.info(f"{namespace}: Successfully created directory '{thumb_dir}'")
        else:
            logging.error(
                f"{namespace}: Failed to create directory '{thumb_dir}'. Aborting."
            )
            raise Exception
    files = os.listdir(image_directory)
    for f in files:
        # Valid files are JPG or PNG that are not supplemental images.
        image = re.match(r"^.+\.(?:jpg|png)$", f)
        if not image:
            continue
        # Supplemental images have a "-[0-9]+" suffix before the file type.
        # AND a file without that suffix exists int he image_directory.
        suffix = re.match(r"(^.+)-[0-9]+(\.(?:jpg|png))$", f)
        if suffix:
            primary = suffix.group(1) + suffix.group(2)
            primary_path = os.path.join(image_directory, primary)
            if os.path.isfile(primary_path):
                continue
        thumb_file = os.path.join(thumb_dir, f)
        big_file = os.path.join(image_directory, f)
        _mkthumb(big_file, thumb_file)


def get_sheet_data(workbook: str, worksheet: str | None = None) -> list[tuple[any]]:
    """
    Get data from a given `workbook`/`worksheet`.

    :param workbook: path to Excel workbook file
    :param worksheet: name of worksheet in workbook file, first worksheet if None
    :returns: The data is returned as a list of tuples where the first item in
    the list contains the keys for the data and the subsequent tuples contain
    the values per row corresponding to those keys.
    """
    namespace = f"{MODULE}.{get_sheet_data.__name__}"
    #########################################################################
    # Try to open sheet_id as an Excel file
    sheet_data = []
    try:
        wb = load_workbook(workbook)
        ws = get_worksheet(wb, worksheet)
        for row in ws.values:
            sheet_data.append(row)
    except (FileNotFoundError, InvalidFileException):
        #########################################################################
        # Google specific stuff
        # authenticate to google sheets
        logging.info(f"{namespace}: Authenticating to google api.")
        creds = app_creds.app_creds()
        sheets_api = build("sheets", "v4", credentials=creds)
        # get sheet data
        if not worksheet:
            sheets = (
                sheets_api.spreadsheets()
                .get(spreadsheetId=workbook)
                .execute()
                .get("sheets", "")
            )
            ws = sheets.pop(0).get("properties", {}).get("title")
        else:
            ws = worksheet
        sheet_data = (
            sheets_api.spreadsheets()
            .values()
            .get(range=ws, spreadsheetId=workbook)
            .execute()
            .get("values")
        )
        #########################################################################
    return sheet_data


def convert_dimensions(dimension: str) -> tuple[str, str, str]:
    """
    Converts string dimension into width, length, & height.
    Also converts cm to inches.

    Only works for dimensions scraped for Amazon or AmazonUK

    Example Dimensions:

    "9.84 x 10.63 inches" (width x height)
    >>> convert_dimensions("9.84 x 10.63 inches")
    ('9.84', '', '10.63')

    "1.0 x 2.54 cm" cm to inch conversion
    >>> convert_dimensions("1.0 x 2.54 cm")
    ('0.39', '', '1.0')

    "5.28 x 0.87 x 7.95 inches" (width x length x height)
    >>> convert_dimensions("5.28 x 0.87 x 7.95 inches")
    ('5.28', '0.87', '7.95')

    "" (no dimension was found)
    >>> convert_dimensions("")
    ('', '', '')

    :param: dimension string value
    :returns: (width, length, height)
    """
    width, length, height = "", "", ""
    standard_dimension_count = 3
    needs_unit_conversion = False
    if re.search(r"cm", dimension):  # dimensions in cm
        dimension = re.sub(r"\s*cm\s*", "", dimension)
        needs_unit_conversion = True
    if re.search(r"inches", dimension):  # dimension in inches
        dimension = re.sub(r"\s*inches\s*", "", dimension)  # remove measure unit
    parsed_dimensions = re.split(r" x ", dimension)
    if needs_unit_conversion:  # convert cm to inches
        for idx, dim in enumerate(parsed_dimensions):
            parsed_dimensions[idx] = str(round((float(dim) / 2.54), 2))
    if len(parsed_dimensions) > 1:
        if len(parsed_dimensions) < standard_dimension_count:  # 2 dimensions, else 3
            width = parsed_dimensions[0]
            height = parsed_dimensions[1]
        else:
            width = parsed_dimensions[0]
            length = parsed_dimensions[1]
            height = parsed_dimensions[2]
    return width, length, height


def get_percentage_discount_prices(
    price: str | int | float, *discount_percentages: str
) -> dict[str, float]:
    """
    Construct a dictionary of discounted prices on a given price
    for any number of given discount percentages.

    Example:

    >>> get_percentage_discount_prices(10.00, "10%", "50%")
    {'10%': 9.0, '50%': 5.0}

    :param price: price to apply discounts to
    :param discount_percentages: discounts expressed in a form like "50%"
    :returns: dictionary of discounted prices keyed by percentage
    """
    price = float(price)
    discount_percent_dict = {}
    for discount in discount_percentages:
        f_disc = float(discount.strip("%")) / 100
        discount_percent_dict[discount] = price - (price * f_disc)
    return discount_percent_dict


def waves_set_pound_price(
    headers: list[str],
    item: item.Item,
    row: tuple[Cell, ...],
    map_scheme: dict = CFG["asg"]["spreadsheet"]["sheet_waves"]["pound_pricing_map"],
    unmapped_multiplier: float = CFG["asg"]["spreadsheet"]["sheet_waves"][
        "pound_pricing_unmapped_multipier"
    ],
) -> None:
    """
    Set 'Pound Pricing' (£) price in appropriate cell of openpyxl spreadsheet row.

    :param headers: list of spreadsheet column headers
    :param item: item.Item object that may have RRP set,
    :param row: openpyxl row of spreadsheet
    :param map_scheme: dictionary of pricing schemes
    :param unmapped_multiplier: default pricing multiplier
    """
    rrp = item.data.get("RRP")
    book_format = item.data.get("FORMAT")
    if rrp and "Pound Pricing" in headers:
        try:
            rrp = float(rrp)
        except ValueError:
            return
        pounds, pence = f"{rrp:.2f}".split(".")
        try:
            pound_price = map_scheme[book_format][pounds][pence]
        except KeyError:
            pound_price = rrp * unmapped_multiplier
        pound_price = f"{round(pound_price, 2):.2f}"
        row[headers.index("Pound Pricing")].value = pound_price


def waves_calculate_fields(headers: list[str], row: tuple[Cell], fields: dict) -> None:
    """
    Empty cell values match with key: ""

    Any cell value matches with key: "any"

    :param headers: list of spreadsheet column headers
    :param row: openpyxl row of spreadsheet
    :param fields: A nested dictionary with the following structure:
        | First level: Name of column/field to change for a given row.
        | Second level:
        |     map_from:
        |         Names of columns to access values for a given row.
        |         These values are used as keys, for indexing into map.
        |     map:
        |         Nested dict.
    """
    for field in list(fields.keys()):
        map_from = fields[field]["map_from"]
        field_map = fields[field]["map"]
        if isinstance(map_from, str):
            map_from = [map_from]
        if field in headers:
            current_level = field_map
            for idx, field_key in enumerate(map_from):
                try:
                    field_val = row[headers.index(field_key)].value
                    field_val = "" if field_val is None else field_val
                    if field_val not in current_level:
                        field_val = "any"
                    if idx == len(map_from) - 1:
                        row[headers.index(field)].value = current_level[field_val]
                    else:
                        current_level = current_level[field_val]
                except (KeyError, ValueError):
                    break


def rename_columns(
    ws: Worksheet,
    headers: list[str],
    rename_fields: dict = CFG["asg"]["spreadsheet"]["sheet_waves"]["rename_fields"],
) -> None:
    """
    Rename `worksheet` columns given mapping in `rename_fields`.

    :param ws: openpyxl worksheet to update
    :param headers: list of column headers
    :rename_fields: dictionary mapping old_name to new_name column names
    """
    for old_name, new_name in rename_fields.items():
        if old_name in headers:
            col_idx = headers.index(old_name) + 1
            ws.cell(row=1, column=col_idx, value=new_name)


def add_waves_row_data(
    headers: list[str],
    item: item.Item,
    row: tuple[Cell, ...],
    gbp_to_usd: float | None,
    **kwargs: any,
) -> None:
    """
    Adds width, height, length, discounts,
    description, images, and preset fields to a row.

    :param headers: list of spreadsheet column headers
    :param item: item.Item object that may have RRP set,
    :param row: openpyxl row of spreadsheet
    :param gbp_to_usd: conversion rate
    :param **kwargs: various sheet_waves key value pairings
    """
    # convert dimension to w, h, l
    width, length, height = convert_dimensions(item.data["DIMENSION"])
    # set new dimension columns
    if "Width" in headers:
        row[headers.index("Width")].value = width
    if "Height" in headers:
        row[headers.index("Height")].value = height
    if "Length" in headers:
        row[headers.index("Length")].value = length
    rrp = item.data.get("RRP")
    if rrp and gbp_to_usd:  # set discounted prices
        discount_prices = get_percentage_discount_prices(
            rrp, *kwargs["discounted_prices"]
        )
        for disc, val in discount_prices.items():
            disc_idx = headers.index(kwargs["discount_text_map"].format(t=disc))
            disc_price = val * gbp_to_usd
            row[disc_idx].value = f"{round(disc_price, 2):.2f}"
    waves_set_pound_price(headers, item, row)
    for idx, img in enumerate(kwargs["image_columns"]):  # add image urls
        try:
            row[headers.index(img)].value = item.image_urls[idx]
        except IndexError:
            break
    if "Description" in headers:
        row[headers.index("Description")].value = item.data.get("DESCRIPTION")
    for field, val in kwargs["preset_fields"].items():
        row[headers.index(field)].value = val
    fields = kwargs["calculate_fields"]
    waves_calculate_fields(headers, row, fields)


def sheet_waves(
    vendor_code: str,
    workbook: str,
    worksheet: str,
    out: str,
    scraped_items_db: str,
    gbp_to_usd: str | float,
    **kwargs: any,
) -> Worksheet:
    """
    Create a new spreadsheet file based on a given `workbook`/`worksheet`
    with added columns and data based on waves preferences.

    The new spreadsheet is saved with the filename(path) given by the `out` parameter.

    :param vendor_code: Vendor code used to lookup isbn_key (See `vendor`)
    :param workbook: path to Excel workbook file
    :param worksheet: name of worksheet in workbook file
    :param out: path where the new spreadsheet will be saved
    :param scraped_items_db: path to scraped items database file
    :param gbp_to_usd: conversion rate
    :param **kwargs: various sheet_waves key value pairings
        (canonically in CFG["asg"]["spreadsheet"]["sheet_waves"])
    :returns: updated Worksheet
    """
    namespace = f"{MODULE}.{sheet_waves.__name__}"

    for discount in kwargs["discounted_prices"]:
        kwargs["data_columns"].append(kwargs["discount_text_map"].format(t=discount))
    for field in kwargs["preset_fields"]:
        kwargs["data_columns"].append(field)
    addl_columns = kwargs["data_columns"] + kwargs["image_columns"]
    # get vendor info from database
    logging.debug(f"{namespace}: Instantiate vendor.")
    vendr = vendor(vendor_code)

    isbn_key = vendr.isbn_key
    logging.debug(f"{namespace}: Setting ISBN_KEY to '{isbn_key}'.")

    sheet_data = get_sheet_data(workbook, worksheet)

    sheet_keys = [x for x in sheet_data.pop(0) if x]  # filter out None
    items_obj = items.Items(sheet_keys, sheet_data, vendr.isbn_key)
    items_obj.load_scraped_data(scraped_items_db)

    # Load worksheet
    logging.info(f"{namespace}: Workbook is {workbook}")
    wb = load_workbook(workbook)
    ws = get_worksheet(wb, worksheet)
    logging.info(f"{namespace}: Worksheet is {ws.title}")

    # Append columns
    col_insert_idx = ws.max_column + 1
    ws.insert_cols(col_insert_idx, len(addl_columns))
    i = 1
    for col in addl_columns:
        col_idx = col_insert_idx + i
        ws.cell(row=1, column=col_idx, value=col)
        i = i + 1

    # Find ISBN column
    row01 = ws[1]
    isbn_idx = None
    for cell in row01:
        if isinstance(cell.value, str) and cell.value.upper() == isbn_key.upper():
            isbn_idx = cell.column - 1
            break
    if isbn_idx is None:
        logging.error(f"{namespace}: Err no isbn column in spreadsheet")
        raise Exception
    # get column headers
    headers = [cell.value for cell in row01]
    # Insert data into cells
    for row in ws.iter_rows(min_row=2):
        # get isbn cell
        isbn = str(row[isbn_idx].value)
        # find items_obj matching isbn
        item = items_obj.find_item(isbn)
        if item:
            add_waves_row_data(
                headers,
                item,
                row,
                gbp_to_usd,
                **kwargs,
            )
    rename_columns(ws, headers)
    # Save workbook
    wb.save(out)
    return ws
