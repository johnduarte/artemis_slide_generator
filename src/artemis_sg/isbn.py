import logging
import re
from os.path import basename, splitext

import isbnlib

MODULE = splitext(basename(__file__))[0]


def validate_isbn(isbn: str) -> str:
    """
    Get validated ISBN-13 from given string

    :param isbn:
        String to be validated.
    :returns: A string of validated ISBN-13, or an empty string is not available.

    Example ISBN values:
    "ISBN-13"
    >>> validate_isbn("9780802150493")
    '9780802150493'

    "ISBN-10"
    >>> validate_isbn("0802157009")
    '9780802157003'

    "ISBN-10 with alpha"
    >>> validate_isbn("069102555X")
    '9780691025551'

    "ISBN-13 with added float"
    >>> validate_isbn("9780802150493.03")
    '9780802150493'

    "ISBN-10 with missing leading zero"
    >>> validate_isbn("123456789")
    '9780123456786'

    "ISBN-13 inside of formula string"
    >>> validate_isbn('="9780802150493"')
    '9780802150493'

    "Invalid ISBN"
    >>> validate_isbn("invalid")
    ''
    """
    namespace = f"{MODULE}.{validate_isbn.__name__}"
    valid_isbn = ""
    mod_isbn = str(isbn)
    if isbnlib.is_isbn13(mod_isbn) or isbnlib.is_isbn10(mod_isbn):
        valid_isbn = isbnlib.to_isbn13(mod_isbn)
    else:
        # look for formula value
        m = re.search('="(.*)"', mod_isbn)
        if m:
            mod_isbn = m.group(1)
        # look for float value
        mod_isbn = mod_isbn.split(".", 1)[0]
        # look for value with missing zero(s)
        mod_isbn = mod_isbn.zfill(10)
        if isbnlib.is_isbn13(mod_isbn) or isbnlib.is_isbn10(mod_isbn):
            valid_isbn = isbnlib.to_isbn13(mod_isbn)
        else:
            logging.error(f"{namespace}: Err reading isbn '{isbn}'")
    return valid_isbn
