<p>
    <img height="130"
        src=https://images.squarespace-cdn.com/content/v1/6110970ca45ca157a1e98b76/e4ea0607-01c0-40e0-a7c0-b56563b67bef/artemis.png?format=200w)"
        alt="Artemis logo">
</p>

# Artemis SG

This is a command line python application used to transform spreadsheet data.

Its primary goals are to produce:
* Google Slide decks
* Excel spreadsheets

It is specifically tailored for the item data and business uses of Artemis Book Sales.

For more information, visit the gitlab pages
[documentation site](https://johnduarte.gitlab.io/artemis_sg/).
